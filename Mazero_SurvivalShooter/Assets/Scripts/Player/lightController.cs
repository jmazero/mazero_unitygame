﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightController : MonoBehaviour
{

    public float lightDamage = 30f;
    public float damTimer = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (damTimer > 0)
        {
            damTimer -= Time.deltaTime;
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (this.gameObject.tag == "Hellephant" && other.gameObject.tag == "Hellephant")
        {
            EnemyHealth enemyHealth = other.GetComponent<EnemyHealth>();
            if (damTimer <= 0)
                {
                enemyHealth.TakeDamage(lightDamage, other.transform.position);
                damTimer = 1;
            }
        }

        if (this.gameObject.tag == "Bear" && other.gameObject.tag == "Bear")
        {
            EnemyHealth enemyHealth = other.GetComponent<EnemyHealth>();
            if (damTimer <= 0)
            {
                enemyHealth.TakeDamage(lightDamage, other.transform.position);
                damTimer = 1;
            }
        }

        if (this.gameObject.tag == "Bunny" && other.gameObject.tag == "Bunny")
        {
            EnemyHealth enemyHealth = other.GetComponent<EnemyHealth>();
            if (damTimer <= 0)
            {
                enemyHealth.TakeDamage(lightDamage, other.transform.position);
                damTimer = 1;
            }
        }

    }

}
