﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{
    public float timeBetweenAttacks = 0.5f;
    public int attackDamage = 10;


    Animator anim;
    GameObject player;
    //reference to player script PlayerHealth
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    //can enemy attack
    bool playerInRange;
    float timer;


    void Awake ()
    {
        //initialize variables
        player = GameObject.FindGameObjectWithTag ("Player");
        playerHealth = player.GetComponent <PlayerHealth> ();
        enemyHealth = GetComponent<EnemyHealth>();
        anim = GetComponent <Animator> ();
    }


    void OnTriggerEnter (Collider other)
    {
        //if enemy collides with player, enable playerinrange
        if(other.gameObject == player)
        {
            playerInRange = true;
        }
    }


    void OnTriggerExit (Collider other)
    {
        //if enemy is not colliding with player anymore, disable playerinrange
        if(other.gameObject == player)
        {
            playerInRange = false;
        }
    }


    void Update ()
    {
        //start accumulating time
        timer += Time.deltaTime;

        //if it has been long enough between attacks. measured by timeBetweenAttacks, and if the player is in range
        if(timer >= timeBetweenAttacks && playerInRange && enemyHealth.currentHealth > 0)
        {
            Attack ();
        }

        //trigger player dead parameter in animator
        if(playerHealth.currentHealth <= 0)
        {
            anim.SetTrigger ("PlayerDead");
        }
    }


    void Attack ()
    {
        //we are now attacking, so reset the timer
        timer = 0f;

        //if player is alive, call takedamage function and pass in the amount of damage we want.
        if(playerHealth.currentHealth > 0)
        {
            playerHealth.TakeDamage (attackDamage);
        }
    }
}
