﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    //the player's transform
    public Transform target;
    //smoothing for lerp function
    public float smoothing = 5f;

    Vector3 offset;

    private void Start()
    {
        //calculate offset of camera
        offset = transform.position - target.position;
    }

    private void FixedUpdate()
    {
        //camera will be at transform of the player plus the offset
        Vector3 targetCamPos = target.position + offset;
        //set the camera position to interpolate between camera and target camera position
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
    }

}
