﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float speed = 6f;

    //for movement
    Vector3 movement;
    //reference to animator
    Animator anim;
    //reference to player rigid body
    Rigidbody playerRigidBody;
    //this is to tell what the raycast to detect. layer masks are stored as integers
    int floorMask;
    //length of raycast
    float camRayLength = 100f;

    private void Awake()
    {
        //this gets the layer that the floor is on and assigns it to floorMask
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent<Animator>();
        playerRigidBody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        //get axis raw gets a value of 0, 1, or -1. unlike getaxis which can get values in between 0,1,-1
        //this makes movement snappier as it immediately returns 1,0,-1
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Move(h, v);
        Turning();
        Animating(h,v);
    }

    //move function that takes our axis values
    void Move(float h, float v)
    {
        //set the vector 3 with the axis values
        movement.Set(h, 0f, v);

        //framerate independent movement that is always at 1
        //this makes it so if you move diagonally you dont have an unfair advantage
        movement = movement.normalized * speed * Time.deltaTime;

        //apply the movement to the player. current position plus the movement
        playerRigidBody.MovePosition(transform.position + movement);
    }

    void Turning()
    {
        //takes point on screen and casts ray to that point. the point in this case is mouse.
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;

        //camray is the ray were using. out is the information we get out of this function, stored in floorhit
        //camraylength is how far were gonna do this raycast, and floormask is the layer were interacting with
        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            //floorhit point minus player transform gives us player to mouse distance
            Vector3 playerToMouse = floorHit.point - transform.position;
            //do not let player lean back
            playerToMouse.y = 0f;

            //lookrotation makes the forward axis of the player the mouse
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            //apply rotation to player
            playerRigidBody.MoveRotation(newRotation);
        }
    }

    void Animating(float h, float v)
    {
        //if h and v are not 0 then we are walking
        bool walking = h != 0f || v != 0f;
        anim.SetBool("IsWalking", walking);
    }

}
