﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightManager : MonoBehaviour
{
    public List<GameObject> lightObjects;
    private int arrayNum = 3;

    // Start is called before the first frame update
    void Start()
    {
        lightObjects[3].SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.E))
        {
            //set the current position to false
            lightObjects[arrayNum].SetActive(false);

            //add one to the arraynum
            arrayNum++;


            if (arrayNum == 4)
            {
                arrayNum = 0;
            }

            //set the current position to active after i increment
            lightObjects[arrayNum].SetActive(true);

        }

    }

}
