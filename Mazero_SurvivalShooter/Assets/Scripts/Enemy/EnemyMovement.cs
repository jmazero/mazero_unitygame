﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    //what enemy moves towards
    Transform player;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    //reference to nav agent on bunny
    UnityEngine.AI.NavMeshAgent nav;


    void Awake ()
    {
        //find player's game object and get transform
        player = GameObject.FindGameObjectWithTag ("Player").transform;
        playerHealth = player.GetComponent <PlayerHealth> ();
        enemyHealth = GetComponent <EnemyHealth> ();
        //gets the nav agent
        nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();
    }


    void Update ()
    {
        if (enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
        {
            //go towards the player
            nav.SetDestination (player.position);
        }
        else
        {
            nav.enabled = false;
        }
    }
}
