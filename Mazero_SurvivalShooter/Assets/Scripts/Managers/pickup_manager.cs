﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pickup_manager : MonoBehaviour
{
    public GameObject pillParent;
    public GameObject bearLight;
    public GameObject hellephantLight;
    public GameObject bunnyLight;
    public ParticleSystem pillParticleSystem;

    private Light bearBulb;
    private Light hellephantBulb;
    private Light bunnyBulb;

    private SphereCollider bearCollider;
    private SphereCollider hellephantCollider;
    private SphereCollider bunnyCollider;

    public static int pillCount = 0;

    public GameObject portalObject;

    public Text warningText;

    public GameObject arrowobject;

    // Start is called before the first frame update
    void Start()
    {
        bearBulb = bearLight.GetComponent<Light>();
        hellephantBulb = hellephantLight.GetComponent<Light>();
        bunnyBulb = bunnyLight.GetComponent<Light>();

        bearCollider = bearLight.GetComponent<SphereCollider>();
        hellephantCollider = hellephantLight.GetComponent<SphereCollider>();
        bunnyCollider = bunnyLight.GetComponent<SphereCollider>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            bearBulb.spotAngle *= 1.2f;
            hellephantBulb.spotAngle *= 1.2f;
            bunnyBulb.spotAngle *= 1.2f;

            bearCollider.radius *= 1.2f;
            bunnyCollider.radius *= 1.2f;
            hellephantCollider.radius *= 1.2f;

            Instantiate(pillParticleSystem, this.transform.position, pillParticleSystem.transform.rotation);

            pillCount += 1;

            if (pillCount >= 3)
            {
                portalObject.SetActive(true);
                warningText.gameObject.SetActive(true);
                arrowobject.SetActive(true);
                StartCoroutine(LateCall(3f));
            }

            Destroy(pillParent);

        }

    }

    IEnumerator LateCall(float waittime)
    {
        Debug.Log("coroutine started");
        yield return new WaitForSeconds(waittime);
        if (warningText.gameObject.activeInHierarchy)
        {
            warningText.gameObject.SetActive(false);
        }

    }

}
